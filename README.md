# Packet X-ing

 A simple packet sniffer and attacking tool. The name is pronounced "Packet Crossing".
 
 # Installation
 
 Basic steps:
 1. Download and extract the source code, or use `git` to clone
 2. then navigate to the folder
 3. and compile.

 # Steps for Debian-based Linux distros, using git:
 ## Download source code
 1. Install git: 
    - Open a **bash** terminal by looking for **Terminal** in apps.
    - Install git with `apt-get`: `sudo apt-get install git`
 2. Download source code into the folder you want:
    - navigate to the folder you want using `cd` in terminal.
    - Use git:
        - command: `git clone <repo>`
        - command including directory: `git clone <repo> <directory>`
        - example: `git clone https://gitlab.com/REDSEA2019/packet-x-ing.git ~/Documents/repos/`
 ## Compile
 3. Install the compiler:
    - Try to use the package manager: Run `sudo apt-get install drracket`
    - If that fails, download the installer script https://racket-lang.org/download/ and run it:
        - Navigate to the folder with the downloaded script. Example: `cd ~/Downloads`
        - Run the script: `./script.sh` . Replace script with the script name.
            - If this fails because you need file permissions, change file permissions: `chmod a+x script.sh`. This gives **a**ll users e**x**ecute privileges on the file.
                - If you can't change file permissions because you need admin privileges, use `sudo`: `sudo chmod a+x script.sh`
            - If running `script.sh` fails because you need privileges, use `sudo`: `sudo ./script.sh`.
 4. To compile, either use the Dr Racket app or `raco` at the command line:
    - Dr Racket:
        1. Open `Packet X-ing.rkt` file in the DrRacket app
        2. Click Run
    - `raco`:
        1. Open a command line (`bash` or **terminal** app).
        2. Navigate to the source directory:
            `cd ~/Documents/repos/packet-x-ing`
        3. Compile to byte-code:
            `raco make "Packet X-ing.rkt"`
            - This compiles to byte-code which the Racket virtual machine executes.
            - To run:
                1. Navigate to the "compiled" subdirectory:
                    `cd compiled`
                2. Run the .zo file with raco:
                    `raco "Packet X-ing.zo"`
        - To compile to a stand-alone executable instead of .zo file:
            - navigate to the project folder 
            - Run: `raco exe --gui "Packet X-ing.rkt"`
            - The executable includes the support files (virtual machine, etc) with the byte-code.